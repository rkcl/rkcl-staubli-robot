# [](https://gite.lirmm.fr/rkcl/rkcl-staubli-robot/compare/v2.0.0...v) (2022-05-13)



# [2.0.0](https://gite.lirmm.fr/rkcl/rkcl-staubli-robot/compare/v1.0.0...v2.0.0) (2021-10-06)


### Bug Fixes

* updated dep versions and fixed errors in app ([2aa1a04](https://gite.lirmm.fr/rkcl/rkcl-staubli-robot/commits/2aa1a044fe6035c69a89daf6a9e650dfd7a17db5))


### Features

* use conventional commits ([05661cc](https://gite.lirmm.fr/rkcl/rkcl-staubli-robot/commits/05661cc1b46ae881de392a04dfc121f8ee88a4ac))



# [1.0.0](https://gite.lirmm.fr/rkcl/rkcl-staubli-robot/compare/v0.1.0...v1.0.0) (2020-03-16)



# 0.1.0 (2020-03-05)



