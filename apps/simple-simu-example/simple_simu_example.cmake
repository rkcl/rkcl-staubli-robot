declare_PID_Component(
    EXAMPLE_APPLICATION
    NAME simple-simu-example
    DIRECTORY simple-simu-example
    RUNTIME_RESOURCES staubli_models staubli_config staubli_logs
    DEPEND
        rkcl-staubli-robot/rkcl-staubli-robot
        rkcl-driver-vrep/rkcl-driver-vrep
        rkcl-otg-reflexxes/rkcl-otg-reflexxes
        pid-os-utilities/pid-signal-manager
        rkcl-app-utility/rkcl-app-utility
)